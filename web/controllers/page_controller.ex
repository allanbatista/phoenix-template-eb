defmodule PhoenixTemplateEb.PageController do
  use PhoenixTemplateEb.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
