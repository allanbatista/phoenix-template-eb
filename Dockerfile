FROM allanbatista/phoenix:0.5

ENV DEBIAN_FRONTEND=noninteractive
ENV MIX_ENV=prod

ADD . /opt/app
WORKDIR /opt/app

RUN mix deps.get --only prod \
    && npm install \
    && bundle install

EXPOSE 4000

CMD mix compile \
    && brunch build --production \
    && mix phoenix.digest \
    && mix ecto.migrate \
    && mix phoenix.server